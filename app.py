from __future__ import absolute_import, division, print_function, unicode_literals
from urllib.parse import urlencode
from urllib.request import Request, urlopen
from pprint import pprint
from starlette.applications import Starlette
from starlette.middleware import Middleware
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import UJSONResponse
import uvicorn
import os
import gc
import scipy
import nltk
import re
import requests as rq
from bs4 import BeautifulSoup
from django.core.validators import URLValidator
from django.core.exceptions import ValidationError
nltk.download('punkt')
nltk.download('averaged_perceptron_tagger')
from nltk.corpus import stopwords
nltk.download('stopwords')
stopset = set(stopwords.words('english'))
from nltk.tokenize import RegexpTokenizer
tokenizer = RegexpTokenizer(r'\w+')
# from sentence_transformers import SentenceTransformer
# model = SentenceTransformer('distilbert-base-nli-mean-tokens')
import spacy 
import en_core_web_md
nlp = en_core_web_md.load()
import random
import json
import string
import markovify

# Firebase
import firebase_admin
from firebase_admin import credentials

from firebase_admin import firestore

cred = credentials.Certificate("firebase.json")
firebase_admin.initialize_app(cred, {
    'databaseURL': 'https://fabalt-8b425.firebaseio.com/'
})
db = firestore.client()
from google.cloud.firestore_v1 import ArrayUnion



# Markov Backup
# Get raw text as string.
with open("./data/adlib.txt") as f:
    markov = f.read()

# Build the model.
markov_model = markovify.Text(markov)



# GPT2 URL
gpt2 = 'https://fabalt-gpt-idvgffrwca-ez.a.run.app' # Set destination URL here

# ref = db.collection('workshops')

# These are the categories for our bag of words
categories = ['JJ', 'NN', 'NNS', 'VBD', 'VBG', 'VB']
badwords = ['wikipedia', 'bbc', 'subscribe', 'messenger', 'twitter', 'facebook', 'google', 'photograph', 'alamy']

prefabInventions = {}
with open('./data/prefab.json') as json_file:
    data = json.load(json_file)
    prefabInventions = data


# Needed to avoid cross-domain issues
response_header = {
    'Access-Control-Allow-Origin': '*'
}
origins = [
    "http://fabalternatives.design",
    "https://fabalternatives.design",
    "http://localhost",
    "http://localhost:3000",
    "http://fabalt-frontend.netlify.com",
    "https://fabalt-frontend.netlify.com",
]


middleware = [
    Middleware(CORSMiddleware,    
    allow_origins=["*"],
    allow_methods=["*"],
    allow_headers=["*"])
]


headers = rq.utils.default_headers()
headers.update({ 'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:52.0) Gecko/20100101 Firefox/52.0'})


generate_count = 0
app = Starlette(debug=True, middleware=middleware)


def randomString(stringLength=4):
    """Generate a random string of fixed length """
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))



# ==--=-=-=-=-==-=-=--=-==-=--==-=--=-=-==--==-=-=--==-==-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--==-=-=--=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# WEB_SCRAPER


def getTextFromURL(url, titles):
    res = rq.get(url)
    html_page = res.content
    soup = BeautifulSoup(html_page, 'html.parser')
    text = soup.find_all(text=True)
    output = ''
    statements = []
    soup.findAll(attrs={"name":"description"})
    
    title = soup.title.string
    titles.append(title)

    print(titles)


    blacklist = [
        '[document]',
        '[endif]'
        '[if]'
        'noscript',
        'header',
        'html',
        'meta',
        'head', 
        'input',
        'script',
        'div',
        'style',
        
        # there may be more elements you don't want, such as "style", etc.
    ]

    for t in text:
        if t.parent.name not in blacklist:
            output += '{} '.format(t.lower())
    return output


def getTextFromURLs(url1 = "", url2 = "", url3 = ""):

    output = ''
    statements = []
    titles = []
    if(url1 != ""):
        try:
            URLValidator()(url1)
            output += '{} '.format(getTextFromURL(url1, titles))

            
        except ValidationError:
            print(ValidationError)

    if(url2 != ""):
        try:
            URLValidator()(url2)
            output += '{} '.format(getTextFromURL(url2, titles))

            
        except ValidationError:
            print(ValidationError)

    if(url3 != ""):
        try:
            URLValidator()(url3)
            output += '{} '.format(getTextFromURL(url3, titles))

            
        except ValidationError:
            print(ValidationError)

    return output, titles

def getSentences(text):
    sentence_list = nltk.sent_tokenize(text)

    cleaned_sentences = []

    if len(sentence_list) < 500:
        for sentence in sentence_list:
            if len(sentence) < 300 and len(sentence) > 50:
                clean_text = re.sub(r'\[[0-9]*\]', ' ', sentence)
                clean_text = re.sub(r'\s+', ' ', clean_text)
                cleaned_sentences.append(clean_text)
    else:
        for sentence in sentence_list:
            if len(sentence) < 300 and len(sentence) > 150:
                clean_text = re.sub(r'\[[0-9]*\]', ' ', sentence)
                clean_text = re.sub(r'\s+', ' ', clean_text)
                cleaned_sentences.append(clean_text)

    #print(len(cleaned_sentences), "sentences found")

    return cleaned_sentences



def getStartingWords(sentence, sentence_dict):
    tokens=tokenizer.tokenize(str(sentence))
    for w in tokens:
      if not w in stopset and len(w)>4:
          tag = nltk.pos_tag([w])
          if tag[0][1] in categories:
            if not tag[0][1] in sentence_dict:
              sentence_dict[tag[0][1]] = []

            if not w in sentence_dict[tag[0][1]]:
              sentence_dict[tag[0][1]].append(w)


def wordsForMultipleURLS(url1 = "", url2 = "", url3 = "", problemStatement = ""):
    #print(problemStatement)
    words = {'JJ':[], 'NN': [], 'VBG':[], 'VBD': [], 'NNS': [], 'VB':[]}

    return problemStatement

def checkIfRelevant(adlib, query):
    adlib_embeddings = nlp(adlib)
    query_embeddings = nlp(query)
    distance = query_embeddings.similarity(adlib_embeddings)
    # #print(distance)
    return distance


def getPairs(phrase, search_tags=['JJ', 'NN']):
    '''
    Iterate over pairs of JJ-NN.
    '''
    tagged = nltk.pos_tag(nltk.word_tokenize(phrase))
    for ngram in ngramise(tagged):
        tokens, tags = zip(*ngram)
        if tags == (search_tags[0], search_tags[1]):
            return tokens

def ngramise(sequence):
    '''
    Iterate over bigrams and 1,2-skip-grams.
    '''
    for bigram in nltk.ngrams(sequence, 2):
        yield bigram
    for trigram in nltk.ngrams(sequence, 3):
        yield trigram[0], trigram[2]


def otherWhatif(sentence_list, returnList):
    # From pairs
    pairs = []
    adlibs = []

    for sentence in sentence_list:
        pair1 = getPairs(sentence['sentence'], ['JJ', 'NN'])
        pair2 = getPairs(sentence['sentence'], ['VBD', 'NN'])
        if (pair1 is not None):
            if len(pair1[0])>5 and len(pair1[1])>5:
                pairs.append(pair1)

        if pair2 is not None:
            if len(pair2[0])>5 and len(pair2[1])>5:
                pairs.append(pair2)
    
    for i in range(0,25):
        choices = random.sample(pairs,2)

        choice1 = ''
        if choices[0][0][0] in 'aeiou':
          choice1 = 'an ' + choices[0][0] + " " + choices[0][1]
        else:
          choice1 = 'a ' + choices[0][0] + " " + choices[0][1]

        choice2 = ''
        if choices[1][0][0] in 'aeiou':
          choice2 = 'an ' + choices[1][0] + " " + choices[1][1]
        else:
          choice2 = 'a ' + choices[1][0] + " " + choices[1][1]

        
        sentence = "What if " + choice1 + " was " + choice2 + "?" 
        print(sentence)
        if sentence not in returnList:
            adlibs.append(sentence)

    return adlibs
    

def generateWhatif(starting_words,query, sentence_list):



#   pprint(sentence_list)
  adlibs = []

  choiceWords = []
  if 'JJ' in starting_words:
    choiceWords += starting_words['JJ']
  if 'NN' in starting_words:
    choiceWords += starting_words['NN']

  if 'VBD' in starting_words:
    choiceWords += starting_words['VBD'] 

  for i in range(100):
    coinToss = random.random()

    sentence = "What if"

    # if we have material and coin toss use VBG option
    if coinToss < 0.33 and 'VBG' in starting_words:
      choice = random.choice(choiceWords)
      nouns = random.sample(starting_words['NN'],2)
      if choice[0] in 'aeiou':
        choice = 'an ' + choice
      else:
        choice = 'a ' + choice
      
      vbg = random.choice(starting_words['VBG'])

      if nouns[1][0] in 'aeiou':
        nouns[1] = ' an ' + nouns[1]
      else:
        nouns[1] = ' a ' + nouns[1]


      sentence = sentence + " " + choice + " " + nouns[0] + " was " + vbg + nouns[1] + "?"

    else:
      choices = random.sample(choiceWords,2)


      # Deal with plural option
      connecting = " was "

      if coinToss >= 0.33 and coinToss < 0.66 and 'NNS' in starting_words:
        nouns = random.sample(starting_words['NNS'],2)
        connecting = " were "


      else:
        nouns = random.sample(starting_words['NN'],2)
        # Check for vowel
        if choices[0][0] in 'aeiou':
          choices[0] = 'an ' + choices[0]
        else:
          choices[0] = 'a ' + choices[0]

        if choices[1][0] in 'aeiou':
          choices[1] = 'an ' + choices[1]
        else:
          choices[1] = 'a ' + choices[1]



      # Check for duplicates in nouns
      if nouns[0] == choices[0]:
        choices[0] = random.choice(starting_words["JJ"])
      if nouns[1] == choices[1]:
        choices[1] = random.choice(starting_words["JJ"])
      



      sentence = sentence + " " + choices[0] + " " + nouns[0] + connecting + choices[1] + " " + nouns[1] + "?"


    if sentence not in adlibs:
        if checkIfRelevant(sentence,query) > 0.5:
            adlibs.append(sentence)







  try:
    moreAdlibs = otherWhatif(sentence_list, adlibs)
    adlibs = adlibs + moreAdlibs
  except:
      print("error generating more adlibs")
#   print(adlibs)  
  return adlibs
  
def generateInventions(starting_words,query):
    adlibs = []
    for i in range(100):
        invention = "Is it "
        nn = random.choice(starting_words['NN']+starting_words['NNS'])
        adj = random.choice(prefabInventions['adjectives'])
        verb = random.choice(prefabInventions["verbs"])
        prefabNoun = random.choice(prefabInventions["nouns"])

        invention = invention + adj["word"] + " " + nn + " that " + verb["word"] + " " + prefabNoun["word"] + "?"
        if invention not in adlibs:
            if checkIfRelevant(invention,query) > 0.5:
                adlibs.append(invention)


    return adlibs


def stripToLastPunc(sentence):

  for idx,char in enumerate(sentence[::-1]):
    if (re.match('^[?.!]$', char) is not None):
      print(char, int(len(sentence)-1) - idx) 
      return sentence[0:int(len(sentence)-1) - idx + 1]

def healthCheck(workshop):

    
    if len(workshop["inventions"])<10 or len(workshop["whatif"])<10 or len(workshop["words"]["NN"])<5:
        return False
    else:
        return True


# ==--=-=-=-=-==-=-=--=-==-=--==-=--=-=-==--==-=-=--==-==-=-=-=-=-=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=--==-=-=--=-=-=-==-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-
# API

@app.route('/api/setup', methods=['GET', 'POST', 'HEAD'])
async def homepage(request):
    workshop_ref = db.collection(u'workshops')
    docs = workshop_ref.stream()



    starting_words = {}
    newWorkshop = {}

    if request.method == 'GET':
        params = request.query_params
        #print(params)
    elif request.method == 'POST':
        params = await request.json()
        #print(params)
    elif request.method == 'HEAD':
        return UJSONResponse({'text': ''},
                             headers=response_header)


    workshop_id = params.get('workshop_id')

    url1 = params.get('url1','')
    url2 = params.get('url2','')
    url3 = params.get('url3','')
    title = params.get('title','')

    ephemeral = params.get('ephemeral', '')


    try:
        URLValidator()(url1)        
    except ValidationError:
        print(ValidationError)
        return UJSONResponse({"error": "url not working"},headers=response_header, status_code=400)


    problemStatement = params.get('problem', '')
    if url1=='' or problemStatement=='' or title=='':
        return UJSONResponse({"error": "url or problem not correct"}, headers=response_header, status_code=400)

    webscrape, titles = getTextFromURLs(url1,url2,url3)
    


    web_sentences = getSentences(webscrape)
    if len(web_sentences) < 10:
        return UJSONResponse({"error": "url or problem not correct"},headers=response_header, status_code=400)


    # sentence_embeddings = model.encode(sentences)

    queries = [problemStatement]
    query_embedding = nlp(problemStatement)
    number_top_matches =  30

    # Get GPT2
    sample = {"prefix": problemStatement, "length":30}

    try:
        r = rq.post(gpt2, json={"prefix": problemStatement, "temperature": "0.7", "top_k": "40", "top_p": "0.6"})
        atext = r.json()["text"]

        for sentence in atext:
            sentence = sentence[len(problemStatement):]
            getStartingWords(sentence, starting_words)

    except Exception as e: 
        print(str(e))
    
# -==-=-===-=-=-==---------------
# Get similiar sentences from articles
    distances = []
    for sentence in web_sentences:
        sentence_embedding = nlp(sentence)
        distance = query_embedding.similarity(sentence_embedding)
        distances.append({'distance':distance, 'sentence':sentence})

    results = sorted(distances, key=lambda x: x['distance'], reverse=True)
    #print("\n\n======================\n\n")
    #print(results)
    #print("\n\n======================\n\n")
    #print("Query:", problemStatement)
    #print("\nTop 5 most similar sentences in corpus:")

    for i in range(0,number_top_matches):
        #print("\n\n======================\n\n")

        #print(results[i])
        getStartingWords(results[i]["sentence"], starting_words)


    

    whatifAdlibs = generateWhatif(starting_words,problemStatement, results)
    inventions = generateInventions(starting_words,problemStatement)
    #print(len(inventions))
    code = randomString(4)
    newWorkshop = {'urls': [url1,url2,url3], 'meta': titles, 'ephemeral': 'true', 'id': code,'inventions': inventions, 'whatif': whatifAdlibs, 'words': starting_words, 'problem': problemStatement, 'title': title, u'timestamp': firestore.SERVER_TIMESTAMP}

    pprint(newWorkshop)
    if healthCheck(newWorkshop) == False:
        return UJSONResponse({"error": "problem getting enough data"},headers=response_header, status_code=400)


    if (ephemeral == 'false'):
        newWorkshop['ephemeral'] = 'false'

    # print(newWorkshop)
    
    new_workshop_ref = db.collection(u'workshops').document(code)
    new_workshop_ref.set(newWorkshop)

    workshop_id = str(code)

    gc.collect()
    return UJSONResponse({'message': "Successfully created workshop", 'id': workshop_id},
                         headers=response_header)



@app.route('/api/insight', methods=['GET', 'POST', 'HEAD'])
async def insight(request):
    workshop_ref = db.collection(u'workshops')
    docs = workshop_ref.stream()
    text = []

    if request.method == 'GET':
        params = request.query_params
        #print(params)
    elif request.method == 'POST':
        params = await request.json()
        #print(params)

    workshopID = params.get('id', '')

    if(workshopID==""):
        return UJSONResponse({"error": "no id"}, headers=response_header, status_code=400)





    prefix1 = params.get('prefix1', '')
    prefix2 = params.get('prefix2', '')
    length = params.get('length', 30)
    temperature = params.get('temperature', 0.7)
    top_k = params.get('top_k', 40)
    top_p = params.get('top_p', 0.9)


    if(prefix1=="") or (prefix2==""):
        return UJSONResponse({"error": "no prefix"},headers=response_header, status_code=400)

    fullprefix = str(prefix1+" "+prefix2)



    # try:
    try:
        r = rq.post(gpt2, json={"prefix": fullprefix, "temperature": "0.7", "top_k": "40", "top_p": "0.6"})
        atext = r.json()["text"]

    except Exception as e: 
        for i in range(5):
            sentence = markov_model.make_short_sentence(100)
            if(len(sentence) > 10):
                text.append(sentence)  
    
    if atext:
        for sentence in atext:
            print(sentence)
            sentence = sentence[len(fullprefix):]
            sentence = stripToLastPunc(sentence)
            if(sentence):
                text.append(sentence)
        print(text)


        
    # except Exception as e: 


    try:
        workshopDoc = workshop_ref.document(workshopID)
        newInsight = json.dumps({'cards':[prefix1, prefix2], 'insights': text})
        workshopDoc.update({u'insights': ArrayUnion([newInsight])})
    except:
       
        return UJSONResponse({"error": "error getting workshop and setting insights"},headers=response_header, status_code=400)       



    gc.collect()
    return UJSONResponse({'text': text },
                             headers=response_header)




@app.route('/api/test')
def test(request):
    return UJSONResponse({'message', "sucesss" },
                             headers=response_header)
    # OR return  'Incorrect password. <a href="/">Go back?</a>

if __name__ == '__main__':
    uvicorn.run(app, host='0.0.0.0', port=int(os.environ.get('PORT', 8080)))
